#! /bin/bash -x

apt-get install -y dnsutils parted util-linux

cat > /tmp/nsupdate <<EOF
server ${1}
zone ${2}
update delete $(hostname -s).$2 A
update add $(hostname -s).${2} 3600 A $(hostname -I | cut -d ' ' -f 1)
send
EOF

nsupdate /tmp/nsupdate
echo "supersede domain-name \"${2}\";" | sudo tee -a /etc/dhcp/dhclient.conf
sudo systemctl restart networking

################## /var/lib/docker partition for nodes only #####################

DISK=/dev/sdc
MOUNT_POINT=/var/lib/docker
DISK_LABEL=DOCKER

if [ -e "$DISK" ]; then
    if [ ! -e "${DISK}1" ]; then
        sfdisk $DISK <<< ";"
        partprobe
        while [ ! -e "${DISK}1" ]; do
            sleep 1
        done
    fi
  
    if [ ! -f /dev/disk/by-label/$DISK_LABEL ]; then
        mkfs.ext4 -L $DISK_LABEL "${DISK}1"
    fi
  
    if [ ! -d "$MOUNT_POINT" ]; then
        mkdir -p "$MOUNT_POINT"
        cat >> /etc/fstab <<EOF
LABEL=$DISK_LABEL $MOUNT_POINT ext4 defaults 0 0
EOF
        mount $MOUNT_POINT
    fi
fi
