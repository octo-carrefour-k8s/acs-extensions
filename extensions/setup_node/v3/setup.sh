#!/bin/bash

# $1 = vpodapidns.sci.xpod.carrefour.com:9000
# $2 = caas-ppd   dans le cas de la zone caas-ppd.xpod.carrefour.com
# $3 = k8s-rec    subzone : donne : k8s-rec.caas-ppd.xpod.carrefour.com
# $4 = tenant_id VPOD : ex : k8s
# $5 = secret api dns
# $6 = vpod dns 1
# $7 = vpod dns 2

URLAPI=$1
XPODZONE=$2
XPODSUBZONE=$3
APITENANTID=$4
APISECRET=$5
VPOD_DNS_1=$6
VPOD_DNS_2=$7

HOSTNAME=$(hostname -s)
DESTHOSTNAME=${HOSTNAME}.${XPODSUBZONE}
COMPLETEZONE=${XPODSUBZONE}.${XPODZONE}.xpod.carrefour.com
IP=$(hostname -I | cut -d ' ' -f 1)

if [[ "${XPODSUBZONE}" = "" ]]; then
    DESTHOSTNAME=${HOSTNAME}
    COMPLETEZONE=${XPODZONE}.xpod.carrefour.com
fi

# remove potential previous
curl https://${URLAPI}/api/dns/service/remove/v1/ -k -H "X-c4token: ${APISECRET}" -H "Content-Type: application/json" -X POST -d "{\"tenant_id\":\"${APITENANTID}\",\"zonename\":\"${XPODZONE}\",\"hostname\":\"${DESTHOSTNAME}\"}"
# add current host
curl https://${URLAPI}/api/dns/service/create/v1/ -k -H "X-c4token: ${APISECRET}" -H "Content-Type: application/json" -X POST -d "{\"tenant_id\":\"${APITENANTID}\",\"zonename\":\"${XPODZONE}\",\"hostname\":\"${DESTHOSTNAME}\",\"ip\":\"${IP}\"}"

echo "supersede domain-name \"${COMPLETEZONE}\";" | sudo tee -a /etc/dhcp/dhclient.conf
sudo systemctl restart networking



echo "
nameserver $VPOD_DNS_1
nameserver $VPOD_DNS_2
" > /etc/resolv-kube.conf

################## /var/lib/docker partition for nodes only #####################

DISK=/dev/sdc
MOUNT_POINT=/var/lib/docker
DISK_LABEL=DOCKER

if [ -e "$DISK" ]; then
    if [ ! -e "${DISK}1" ]; then
        sfdisk $DISK <<< ";"
        partprobe
        while [ ! -e "${DISK}1" ]; do
            sleep 1
        done
    fi
  
    if [ ! -f /dev/disk/by-label/$DISK_LABEL ]; then
        mkfs.ext4 -L $DISK_LABEL "${DISK}1"
    fi
  
    if [ ! -d "$MOUNT_POINT" ]; then
        mkdir -p "$MOUNT_POINT"
        cat >> /etc/fstab <<EOF
LABEL=$DISK_LABEL $MOUNT_POINT ext4 defaults 0 0
EOF
        mount $MOUNT_POINT
    fi
fi
