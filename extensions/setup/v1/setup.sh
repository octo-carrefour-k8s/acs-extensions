#! /bin/sh

apt-get install -y dnsutils

cat > /tmp/nsupdate <<EOF
server $1
zone $2
update delete $(hostname -s).$2 A
update add $(hostname -s).$2 3600 A $(hostname -I | cut -d ' ' -f 1)
send
EOF

nsupdate /tmp/nsupdate
echo "supersede domain-name \"$2\";" | sudo tee -a /etc/dhcp/dhclient.conf
sudo systemctl restart networking
