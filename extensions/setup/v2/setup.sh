#!/bin/bash

# $1 = vpodapidns.sci.xpod.carrefour.com:9000
# $2 = caas-ppd   dans le cas de la zone caas-ppd.xpod.carrefour.com
# $3 = k8s-rec    subzone : donne : k8s-rec.caas-ppd.xpod.carrefour.com
# $3 = tenant_id VPOD : ex : k8s
# $4 = secret api dns 

URLAPI=$1
XPODZONE=$2
XPODSUBZONE=$3
APITENANTID=$4
APISECRET=$5

HOSTNAME=$(hostname -s)
DESTHOSTNAME=${HOSTNAME}.${XPODSUBZONE}
COMPLETEZONE=${XPODSUBZONE}.${XPODZONE}.xpod.carrefour.com
IP=$(hostname -I | cut -d ' ' -f 1)

if [[ "${XPODSUBZONE}" = "" ]]; then
	DESTHOSTNAME=${HOSTNAME}
	COMPLETEZONE=${XPODZONE}.xpod.carrefour.com
fi

# remove potential previous
curl https://${URLAPI}/api/dns/service/remove/v1/ -k -H "X-c4token: ${APISECRET}" -H "Content-Type: application/json" -X POST -d "{\"tenant_id\":\"${APITENANTID}\",\"zonename\":\"${XPODZONE}\",\"hostname\":\"${DESTHOSTNAME}\"}"
# add current host
curl https://${URLAPI}/api/dns/service/create/v1/ -k -H "X-c4token: ${APISECRET}" -H "Content-Type: application/json" -X POST -d "{\"tenant_id\":\"${APITENANTID}\",\"zonename\":\"${XPODZONE}\",\"hostname\":\"${DESTHOSTNAME}\",\"ip\":\"${IP}\"}"

echo "supersede domain-name \"${COMPLETEZONE}\";" | sudo tee -a /etc/dhcp/dhclient.conf
sudo systemctl restart networking

