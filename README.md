# How to use the extension

The folder `extensions` must be uploaded on an HTTP server as is.
The json file given to `acs-engine` must fill the `RootURL` extension field, giving the URL of the HTTP server.

## What these extensions do

There are two extensions:
  * setup/v1: this extension declares the VPOD's DNS (dnsupdate) and superseeds the search domain that is not configurable in azure (superseed)
  * setup_node/v1: this extension does the same as the first one and adds also the mount of the disk that is dedicated to docker

  
## How Azure extensions work

In the `kubernetes.json.j2` file, we find:

~~~json
"masterProfile": {
      "dnsPrefix": "{{ dns_prefix }}",
       ...
        `"preProvisionExtension"`: {
          `"name": "setup"`
        }
    },
    "agentPoolProfiles": [
      {
        "name": "workers0",
        ...
        `"preProvisionExtension"`: {
          `"name": "setup_node`"
        }
      }
~~~

And further:

~~~json
"extensionProfiles": [
        {
            "name": "setup_node",
            "version": "v1",
            "script": "setup.sh",
            "rootURL": "https://gitlab.com/octo-carrefour-k8s/acs-extensions/raw/master/",
            "extensionParameters": "{{ misc_dns_ip }} {{ sandbox_domain }}"
        },
        {
            "name": "setup",
            "version": "v1",
            "script": "setup.sh",
            "rootURL": "https://gitlab.com/octo-carrefour-k8s/acs-extensions/raw/master/",
            "extensionParameters": "{{ misc_dns_ip }} {{ sandbox_domain }}"
        }
      ]
~~~

The ACS-ENGINE will try to curl these scripts in the cloudinit (/usr/lib/cloud-init) phase at `https://rootURL/extensions/<name>/<version>/<script>`, for example https://gitlab.com/octo-carrefour-k8s/acs-extensions/raw/master/extensions/setup/v1/setup.sh
